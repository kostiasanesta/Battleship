package by.seabattle.server;


import java.util.Scanner;

public class Application {
    private Scanner scanner;
    public static void main(String[] args){
        Application app = new Application();
        app.runTheApp();
    }

    private void runTheApp() {
        System.out.println("Enter port number, value should be between 1025 and 65535");
        try {
            Server server = new Server(readIntValue());
            server.start();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            runTheApp();
        }
    }
    private int readIntValue(){
        scanner = new Scanner(System.in);
        if(scanner.hasNextInt()){
            return scanner.nextInt();
        }else{
            System.out.println("enter a number");
            return readIntValue();
        }
    }
}

package by.seabattle.server.threading;


import by.seabattle.messaging.interfaces.IMessageDispatcher;
import by.seabattle.messaging.messages.Message;
import by.seabattle.server.MessageDispatcher;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ClientThread extends Thread {

    private boolean isBusy;
    private boolean isRunning;
    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private int score;
    private String clientName;
    private String clientId;
    private IMessageDispatcher delegate;
    private String oponentId;

    public ClientThread(Socket socket) throws IOException {
        this.socket = socket;
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
        clientId = String.valueOf(System.currentTimeMillis() + "_" + socket.hashCode());
        System.out.println("во время создания серверного потока"+clientId);
    }

    public String getClientId() {
        return clientId;
    }

    public void setDelegate(IMessageDispatcher delegate) {
        this.delegate = delegate;
    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {
            try {
                Message msg = (Message) in.readObject();
                if (delegate != null) {
                    delegate.onMessageReceived(msg, clientId);
                }
                Thread.sleep(100);
            } catch (EOFException e){
                System.out.println(e.getMessage());
                isRunning=false;
            }catch (SocketException e) {
                System.out.println("socket closed!");
                isRunning = false;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                isRunning = false;
            }

        }
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if (delegate != null) {
            if (isBusy) {
                delegate.finishGame(clientId);
            }
            delegate.removeClient(clientId);
        }
    }

    public void sendMessage(Message msg) throws IOException {

        synchronized (out) {
            out.writeObject(msg);
        }
    }

    public void shutDown() {
        isRunning = false;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void setBusy(boolean busy) {
        isBusy = busy;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }


    public String getOponentId() {
        return oponentId;
    }

    public void setOponentId(String oponentId) {
        this.oponentId = oponentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientThread that = (ClientThread) o;

        if (isBusy != that.isBusy) return false;
        if (isRunning != that.isRunning) return false;
        if (score != that.score) return false;
        if (socket != null ? !socket.equals(that.socket) : that.socket != null) return false;
        if (out != null ? !out.equals(that.out) : that.out != null) return false;
        if (in != null ? !in.equals(that.in) : that.in != null) return false;
        if (clientName != null ? !clientName.equals(that.clientName) : that.clientName != null) return false;
        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) return false;
        if (delegate != null ? !delegate.equals(that.delegate) : that.delegate != null) return false;
        return oponentId != null ? oponentId.equals(that.oponentId) : that.oponentId == null;
    }

    @Override
    public int hashCode() {
        int result = (isBusy ? 1 : 0);
        result = 31 * result + (isRunning ? 1 : 0);
        result = 31 * result + (socket != null ? socket.hashCode() : 0);
        result = 31 * result + (out != null ? out.hashCode() : 0);
        result = 31 * result + (in != null ? in.hashCode() : 0);
        result = 31 * result + score;
        result = 31 * result + (clientName != null ? clientName.hashCode() : 0);
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (delegate != null ? delegate.hashCode() : 0);
        result = 31 * result + (oponentId != null ? oponentId.hashCode() : 0);
        return result;
    }
}

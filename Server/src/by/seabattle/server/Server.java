package by.seabattle.server;


import by.seabattle.server.threading.ClientThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    private boolean isRunning;
    private ServerSocket serverSocket;
    private MessageDispatcher messageDispatcher;

    public Server(int port) throws Exception {
        serverSocket = new ServerSocket(port);
        messageDispatcher = new MessageDispatcher();
        System.out.println("server runing on port " +port);
    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning){
            try {
                Socket socket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(socket);
                messageDispatcher.registerClientThread(clientThread);
                clientThread.start();
                Thread.sleep(100);
                System.out.println("new client connected!");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
        }
        try {
            serverSocket.close();
            messageDispatcher.shutDown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package by.seabattle.server;

import by.seabattle.messaging.interfaces.IMessageDispatcher;
import by.seabattle.messaging.messages.*;
import by.seabattle.messaging.units.OnlineUser;
import by.seabattle.server.threading.ClientThread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class MessageDispatcher implements IMessageDispatcher {
    private ConcurrentHashMap<String, ClientThread> clients;

    public MessageDispatcher() {
        clients = new ConcurrentHashMap<>();
    }

    public void registerClientThread(ClientThread client) {
        if (client != null) {
            client.setDelegate(this);
            clients.put(client.getClientId(), client);
        }
    }

    public void shutDown() {
        for (Map.Entry<String, ClientThread> entry : clients.entrySet()) {
            entry.getValue().shutDown();
        }
        clients.clear();
    }

    @Override
    public void onMessageReceived(Message message, String clientId) {
        switch (message.getType()) {
            case Message.MSG_TYPE_ONLINE_USERS:
                processGetOnlineUsersMessage((GetOnlineUsersMessage) message, clientId);
                break;
            case Message.MSG_TYPE_GAME_REQUEST:
                processGetRequestMessage((GameRequestMessage) message, clientId);
                break;
            case Message.MSG_TYPE_GAME_RESPONSE:
                processGameResponseMessage((GameResponseMessage) message, clientId);
                break;
            case Message.MSG_TYPE_GAME_OVER:
                processGameOverMessage((GameOverMessage) message, clientId);
                break;
            case Message.MSG_TYPE_BEAT:
                processBeatMessage((BeatMessage) message, clientId);
                break;
            case Message.MSG_TYPE_WAITING:
                processWaitingMessage((WaitingMessage) message, clientId);
                break;
            case Message.MSG_TYPE_FLEET:
                processGameFleetMessage((GameFleetMessage) message, clientId);
                break;
        }
    }

    private void processGameFleetMessage(GameFleetMessage message, String clientId) {
        clients.get(clientId).setBusy(true);
        try {
            clients.get(clientId).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processWaitingMessage(WaitingMessage message, String clientId) {
        try {
            clients.get(message.getRecipientId()).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void processBeatMessage(BeatMessage message, String clientId) {
        try {
            clients.get(message.getRecipientId()).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void processGameOverMessage(final GameOverMessage message, final String clientId) {
        //clients.get(clientId).setBusy(false);
       // clients.get(message.getRecipientId()).setBusy(false);
        try {
            clients.get(message.getRecipientId()).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                GetOnlineUsersMessage msg = new GetOnlineUsersMessage();
                msg.setOnlineUsers(getOnlineUsersList());
                sendServiceMessage(msg, "none");
            }
        });
        thread.start();
    }

    private void processGameResponseMessage(GameResponseMessage message, String clientId) {
        if (message.isApproved()) {
            clients.get(clientId).setBusy(true);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    GetOnlineUsersMessage msg = new GetOnlineUsersMessage();
                    msg.setOnlineUsers(getOnlineUsersList());
                    sendServiceMessage(msg, clientId);
                }
            });
            thread.start();
        }
        try {
            clients.get(message.getRecipientId()).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processGetRequestMessage(GameRequestMessage message, final String clientId) {
        clients.get(clientId).setBusy(true);
        clients.get(message.getRecipientId()).setBusy(true);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                GetOnlineUsersMessage msg = new GetOnlineUsersMessage();
                msg.setOnlineUsers(getOnlineUsersList());
                sendServiceMessage(msg, clientId);

            }
        });
        thread.start();
        try {
            clients.get(message.getRecipientId()).sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void processGetOnlineUsersMessage(GetOnlineUsersMessage message, String clientId) {
        clients.get(clientId).setBusy(false);
        clients.get(clientId).setClientName(message.getClientName());
        message.setOnlineUsers(getOnlineUsersList());
        try {
            clients.get(clientId).sendMessage(message);
        } catch (IOException e) {
            System.out.println("processGetOnlineUsersMessage" + e.getMessage());
        }

    }


    @Override
    public void removeClient(String clientId) {
        clients.remove(clientId);
    }

    private void sendServiceMessage(Message msg, String clientId) {
        for (Map.Entry<String, ClientThread> entry : clients.entrySet()) {
            if (!entry.getValue().getClientId().equals(clientId)) {
                try {
                    entry.getValue().sendMessage(msg);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    private List<OnlineUser> getOnlineUsersList() {
        List<OnlineUser> list = new ArrayList<>();
        for (Map.Entry<String, ClientThread> entry : clients.entrySet()) {
            list.add(new OnlineUser(entry.getValue().isBusy(), entry.getValue().getScore(),
                    entry.getValue().getClientId(), entry.getValue().getClientName()));

        }
        return list;
    }

    @Override
    public void finishGame(String clientId) {
        try {
            clients.get(clients.get(clientId).getOponentId()).sendMessage(new GameOverMessage(true, false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

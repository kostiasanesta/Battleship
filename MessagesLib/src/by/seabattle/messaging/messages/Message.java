package by.seabattle.messaging.messages;


import java.io.Serializable;

public abstract class Message implements Serializable{
    public Message(int type) {
        this.type = type;
    }
    private static final long serialVersionUID = -6968127693997127446L;

    public static final int MSG_TYPE_ONLINE_USERS   = 1;
    public static final int MSG_TYPE_GAME_REQUEST   = 2;
    public static final int MSG_TYPE_GAME_RESPONSE  = 3;
    public static final int MSG_TYPE_GAME_OVER      = 4;
    public static final int MSG_TYPE_BEAT           = 5;
    public static final int MSG_TYPE_WAITING        = 6;
    public static final int MSG_TYPE_FLEET          = 7;

    private int type;
    protected String senderId;
    protected String recipientId;



    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }
}

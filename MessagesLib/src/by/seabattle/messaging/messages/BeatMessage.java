package by.seabattle.messaging.messages;


import java.util.List;

public class BeatMessage extends Message {

    private static final long serialVersionUID = -3671702849207796793L;

    private int cellIndex;
    private boolean isDamaged;
    private boolean isDestroyed;
    private boolean turnChanger;
    private boolean isShip;


    public BeatMessage() {
        super(Message.MSG_TYPE_BEAT);
    }

    public boolean isShip() {
        return isShip;
    }

    public void setShip(boolean ship) {
        isShip = ship;
    }

    public int getCellIndex() {
        return cellIndex;
    }

    public void setCellIndex(int cellIndex) {
        this.cellIndex = cellIndex;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    public boolean isDestroyed() {
        return isDestroyed;
    }

    public void setDestroyed(boolean destroyed) {
        isDestroyed = destroyed;
    }

    public boolean isTurnChanger() {
        return turnChanger;
    }

    public void setTurnChanger(boolean turnChanger) {
        this.turnChanger = turnChanger;
    }
}

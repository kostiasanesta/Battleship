package by.seabattle.messaging.messages;


public class GameFleetMessage extends Message{
    public GameFleetMessage() {
        super(Message.MSG_TYPE_FLEET);
    }
}

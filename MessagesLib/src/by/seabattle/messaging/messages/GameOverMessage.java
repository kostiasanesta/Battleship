package by.seabattle.messaging.messages;


public class GameOverMessage extends Message {

    private static final long serialVersionUID = -2554920049562213893L;

    private boolean connectionLose;
    private boolean senderWon;

    public GameOverMessage() {
        super(Message.MSG_TYPE_GAME_OVER);
    }

    public GameOverMessage(boolean connectionLose, boolean senderWon) {
        this();
        this.connectionLose = connectionLose;
        this.senderWon = senderWon;
    }

    public boolean isConnectionLose() {
        return connectionLose;
    }

    public void setConnectionLose(boolean connectionLose) {
        this.connectionLose = connectionLose;
    }

    public boolean isSenderWon() {
        return senderWon;
    }

    public void setSenderWon(boolean senderWon) {
        this.senderWon = senderWon;
    }
}

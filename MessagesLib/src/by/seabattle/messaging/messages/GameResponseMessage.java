package by.seabattle.messaging.messages;


public class GameResponseMessage extends Message {


    private static final long serialVersionUID = -5058449486897678459L;
    private boolean isApproved;
    private boolean firstTurn;

    public GameResponseMessage() {
        super(Message.MSG_TYPE_GAME_RESPONSE);
    }

    public boolean isApproved() {
        return isApproved;
    }

    public boolean isFirstTurn() {
        return firstTurn;
    }

    public void setFirstTurn(boolean firstTurn) {
        this.firstTurn = firstTurn;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
}

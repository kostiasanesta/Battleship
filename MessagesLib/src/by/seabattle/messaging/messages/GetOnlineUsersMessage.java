package by.seabattle.messaging.messages;


import by.seabattle.messaging.units.OnlineUser;

import java.util.ArrayList;
import java.util.List;

public class GetOnlineUsersMessage extends Message{

    private static final long serialVersionUID = 3951080311200437999L;

    private List<OnlineUser> onlineUsers;
    private String clientName;

    public GetOnlineUsersMessage(){
        super(Message.MSG_TYPE_ONLINE_USERS);
        onlineUsers = new ArrayList<>();
    }

    public List<OnlineUser> getOnlineUsers() {
        return onlineUsers;
    }

    public void setOnlineUsers(List<OnlineUser> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}

package by.seabattle.messaging.messages;


import by.seabattle.messaging.units.OnlineUser;

public class GameRequestMessage extends Message {

    private static final long serialVersionUID = 1435697317735109268L;
    private OnlineUser onlineUser;

    public GameRequestMessage() {
        super(Message.MSG_TYPE_GAME_REQUEST);
    }

    public OnlineUser getOnlineUser() {
        return onlineUser;
    }

    public void setOnlineUser(OnlineUser onlineUser) {
        this.onlineUser = onlineUser;
    }
}

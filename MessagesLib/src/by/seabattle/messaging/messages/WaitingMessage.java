package by.seabattle.messaging.messages;


public class WaitingMessage extends Message {
    public WaitingMessage() {
        super(Message.MSG_TYPE_WAITING);
    }
}

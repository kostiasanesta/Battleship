package by.seabattle.messaging.interfaces;


import by.seabattle.messaging.messages.Message;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public interface IMessageDispatcher {

    default void onMessageReceived(Message message, String clientId) {
        throw new NotImplementedException();
    }

    default void onMessageReceived(Message message) {
        throw new NotImplementedException();
    }

    default void removeClient(String clientId) {
        throw new NotImplementedException();
    }

    default void finishGame(String clientId) {
        throw new NotImplementedException();
    }

}

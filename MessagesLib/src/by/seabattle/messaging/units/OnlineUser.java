package by.seabattle.messaging.units;

import java.io.Serializable;

public class OnlineUser implements Serializable {

    private static final long serialVersionUID = -1899021109016757691L;

    private boolean isBusy;
    private int score;
    private String userId;
    private String name;

    public OnlineUser(boolean isBusy, int score, String userId, String name) {
        this.isBusy = isBusy;
        this.score = score;
        this.userId = userId;
        this.name = name;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void setBusy(boolean busy) {
        isBusy = busy;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OnlineUser{");
        sb.append("isBusy=").append(isBusy);
        sb.append(", score=").append(score);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

package by.volkov.gamefx.gamecontrolls;


public interface IGameLogicCallbackListener {
    void proccessGameLogicCallback(boolean isDamaged, boolean isDestroyed);
}

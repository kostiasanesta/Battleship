package by.volkov.gamefx.gamecontrolls;


import java.util.ArrayList;
import java.util.List;

public class FourthTypeShip implements ITargetCell {
    private List<GameCell> fourthDeckShip;
    private GameCellType type;
    private String shipIndex;

    public FourthTypeShip(GameCell firstCell, GameCell secondCell, GameCell thirdCell, GameCell fourhCell) {
        fourthDeckShip = new ArrayList<>();
        type = GameCellType.FOURTH_DECK_SHIP;
        shipIndex = Integer.toString(firstCell.getIndex()) + Integer.toString(secondCell.getIndex()) + Integer.toString(thirdCell.getIndex()) + Integer.toString(fourhCell.getIndex())+String.valueOf(type);
        fourthDeckShip.add(0,firstCell);
        fourthDeckShip.add(1,secondCell);
        fourthDeckShip.add(2,thirdCell);
        fourthDeckShip.add(3,fourhCell);
        for (GameCell cell : fourthDeckShip) {
            cell.setShipIndex(shipIndex);
            cell.setType(type);
        }
    }


    @Override
    public List<GameCell> getShip() {
        return fourthDeckShip;
    }

    @Override
    public String getShipIndex() {
        return shipIndex;
    }


    @Override
    public GameCellType getType() {
        return type;
    }
}

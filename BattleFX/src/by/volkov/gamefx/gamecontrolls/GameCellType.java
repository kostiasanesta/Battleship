package by.volkov.gamefx.gamecontrolls;


public enum GameCellType {
    ONE_DECK_SHIP,
    TWO_DECK_SHIP,
    THIRD_DECK_SHIP,
    FOURTH_DECK_SHIP,
    WATER
}

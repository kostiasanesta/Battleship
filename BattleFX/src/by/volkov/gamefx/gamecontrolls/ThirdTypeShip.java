package by.volkov.gamefx.gamecontrolls;


import java.util.ArrayList;
import java.util.List;

public class ThirdTypeShip implements ITargetCell {
    private List<GameCell> thirdDeckShip;
    private GameCellType type;
    private String shipIndex;

    public ThirdTypeShip(GameCell firstCell, GameCell secondCell, GameCell thirdCell) {
        thirdDeckShip = new ArrayList<>();
        type = GameCellType.THIRD_DECK_SHIP;
        shipIndex = Integer.toString(firstCell.getIndex()) + Integer.toString(secondCell.getIndex()) + Integer.toString(thirdCell.getIndex())+String.valueOf(type);
        thirdDeckShip.add(0,firstCell);
        thirdDeckShip.add(1,secondCell);
        thirdDeckShip.add(2,thirdCell);
        for (GameCell cell : thirdDeckShip) {
            cell.setShipIndex(shipIndex);
            cell.setType(type);
        }
    }


    @Override
    public List<GameCell> getShip() {
        return thirdDeckShip;
    }

    @Override
    public String getShipIndex() {
        return shipIndex;
    }


    @Override
    public GameCellType getType() {
        return type;
    }
}

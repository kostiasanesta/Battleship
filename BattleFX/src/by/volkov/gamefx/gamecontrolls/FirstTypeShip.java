package by.volkov.gamefx.gamecontrolls;


import java.util.ArrayList;
import java.util.List;

public class FirstTypeShip implements ITargetCell {
    private List<GameCell> oneDeckShip;
    private GameCellType type;
    private String shipIndex;

    public FirstTypeShip(GameCell cell) {
        oneDeckShip = new ArrayList<>();
        type = GameCellType.ONE_DECK_SHIP;
        shipIndex = Integer.toString(cell.getIndex())+String.valueOf(type);
        oneDeckShip.add(0,cell);
        cell.setShipIndex(shipIndex);
        cell.setType(type);
    }

    @Override
    public List<GameCell> getShip() {
        return oneDeckShip;
    }

    @Override
    public String getShipIndex() {
        return shipIndex;
    }


    @Override
    public GameCellType getType() {
        return type;
    }

}

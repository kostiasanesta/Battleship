package by.volkov.gamefx.gamecontrolls;


import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;


public class GameCell extends Pane implements Drawable{
    private double side;
    private double x, y;
    private int index;
    private GameCellStatus status;
    private GameCellType type;
    private boolean readyToBuild;
    private boolean canBeFired;
    private String shipIndex;
    private boolean isDamaged;


    public GameCell( double side) {
        this.status = GameCellStatus.EMPTY;
        this.side = side;
        readyToBuild = true;
        type=GameCellType.WATER;
        shipIndex = String.valueOf(GameCellType.WATER);
        canBeFired = true;
    }

    @Override
    public void draw(GraphicsContext gc) {
        switch (status){
            case EMPTY:
                drawCell(Color.LIGHTSEAGREEN,gc);
                break;
            case MISS:
                drawCell(Color.LIGHTCORAL,gc);
                gc.setFill(Color.BLACK);
                gc.fillOval(x+(side/2)-(side/8),y+(side/2)-(side/8),side/4,side/4);
                break;
            case SHIP:
                drawCell(Color.SADDLEBROWN,gc);
                break;
            case SHIP_DAMAGED:
                drawCell(Color.ORANGE,gc);
                break;
            case SHIP_DESTROYED:
                drawCell(Color.DIMGRAY,gc);
                break;
                default:
                    break;

        }
    }
    private void drawCell(Color color, GraphicsContext gc){
        gc.setStroke(Color.BLACK);
        gc.strokeRect(x, y, side,side);
        gc.setFill(color);
        gc.fillRect(x+1.0, y+1.0,side,side);
    }



    public void setPoint2D(Point2D point2D){
        x = side*(point2D.getX()+1);
        y = side*(point2D.getY()+1);
    }

    public GameCellStatus getStatus() {
        return status;
    }

    public void setStatus(GameCellStatus status) {
        this.status = status;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReadyToBuild() {
        return readyToBuild;
    }

    public void setReadyToBuild(boolean readyToBuild) {
        this.readyToBuild = readyToBuild;
    }

    public boolean isCanBeFired() {
        return canBeFired;
    }

    public void setCanBeFired(boolean canBeFired) {
        this.canBeFired = canBeFired;
    }

    public GameCellType getType() {
        return type;
    }

    public void setType(GameCellType type) {
        this.type = type;
    }

    public String getShipIndex() {
        return shipIndex;
    }

    public void setShipIndex(String shipIndex) {
        this.shipIndex = shipIndex;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }
}

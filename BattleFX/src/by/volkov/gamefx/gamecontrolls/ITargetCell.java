package by.volkov.gamefx.gamecontrolls;


import java.util.List;

public interface ITargetCell {
    List<GameCell> getShip();
    String getShipIndex();
    GameCellType getType();

}

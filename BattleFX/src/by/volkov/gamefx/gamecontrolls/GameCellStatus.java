package by.volkov.gamefx.gamecontrolls;


public enum GameCellStatus {
    EMPTY,
    MISS,
    SHIP,
    SHIP_DAMAGED,
    SHIP_DESTROYED

}

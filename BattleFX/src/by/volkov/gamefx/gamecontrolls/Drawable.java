package by.volkov.gamefx.gamecontrolls;


import javafx.scene.canvas.GraphicsContext;

public interface Drawable {
    void draw(GraphicsContext gc);
}

package by.volkov.gamefx.gamecontrolls;


import java.util.ArrayList;

import java.util.List;

public class Fleet {
    private int firstNumber;
    private int secondNumber;
    private int thirdNumber;
    private int fourthNumber;
    private List<ITargetCell> fleet;

    public Fleet() {
        fleet = new ArrayList<>();
    }

    public List<ITargetCell> getFleet() {
        return fleet;
    }

    public void toCountShips() {
        firstNumber = 0;
        secondNumber = 0;
        thirdNumber = 0;
        fourthNumber = 0;
        for (ITargetCell cell : fleet) {
            switch (cell.getType()) {
                case FOURTH_DECK_SHIP:
                    fourthNumber++;
                    break;
                case ONE_DECK_SHIP:
                    firstNumber++;
                    break;
                case THIRD_DECK_SHIP:
                    thirdNumber++;
                    break;
                case TWO_DECK_SHIP:
                    secondNumber++;
                    break;
                default:
                    break;
            }
        }
    }

    public void addToFleet(ITargetCell ship) {
        fleet.add(ship);
    }

    public boolean testingToAdd(GameCellType type) {
        toCountShips();
        if (type == GameCellType.FOURTH_DECK_SHIP && fourthNumber <= 0) {
            return true;
        } else if (type == GameCellType.THIRD_DECK_SHIP && (thirdNumber < 2 || (thirdNumber < 3 && fourthNumber <= 0))) {
            return true;
        } else if (type == GameCellType.TWO_DECK_SHIP && (secondNumber < 3 || (secondNumber < 4 && ((fourthNumber <= 0 && thirdNumber <= 2)
                || (fourthNumber <= 1 && thirdNumber <= 1))) || (secondNumber < 5 && ((fourthNumber <= 0 && thirdNumber <= 1) || (fourthNumber <= 1 && thirdNumber <= 0)))
                || (secondNumber < 6 && (fourthNumber <= 0 && thirdNumber <= 0)))) {
            return true;
        } else if (type == GameCellType.ONE_DECK_SHIP && (firstNumber < 4 || (firstNumber < 5 && ((fourthNumber <= 0 && thirdNumber <= 2 && secondNumber <= 3) ||
                (fourthNumber <= 1 && thirdNumber <= 1 && secondNumber <= 3) || (fourthNumber <= 1 && thirdNumber <= 2 && secondNumber <= 2))) || (firstNumber < 6 &&
                ((fourthNumber <= 0 && thirdNumber <= 1 && secondNumber <= 3) || (fourthNumber <= 0 && thirdNumber <= 2 && secondNumber <= 2) || (fourthNumber <= 1 && thirdNumber <= 0 && secondNumber <= 3) ||
                        (fourthNumber <= 1 && thirdNumber <= 1 && secondNumber <= 2) || (fourthNumber <= 1 && thirdNumber <= 2 && secondNumber <= 1))) || (firstNumber < 7 &&
                ((fourthNumber <= 0 && thirdNumber <= 0 && secondNumber <= 3) || (fourthNumber <= 0 && thirdNumber <= 1 && secondNumber <= 2) || (fourthNumber <= 0 && thirdNumber <= 2 && secondNumber <= 1) ||
                        (fourthNumber <= 1 && thirdNumber <= 0 && secondNumber <= 2) || (fourthNumber <= 1 && thirdNumber <= 1 && secondNumber <= 1) ||
                        (fourthNumber <= 1 && thirdNumber <= 2 && secondNumber <= 0))) || (firstNumber < 8 && ((fourthNumber <= 0 && thirdNumber <= 0 && secondNumber <= 2) ||
                (fourthNumber <= 0 && thirdNumber <= 1 && secondNumber <= 1) || (fourthNumber <= 0 && thirdNumber <= 2 && secondNumber <= 0) || (fourthNumber <= 1 && thirdNumber <= 0 &&
                secondNumber <= 1) || (fourthNumber <= 1 && thirdNumber <= 1 && secondNumber <= 0))) || (firstNumber < 9 && ((fourthNumber <= 0 && thirdNumber <= 0 && secondNumber <= 1) ||
                (fourthNumber <= 0 && thirdNumber <= 1 && secondNumber <= 0) || (fourthNumber <= 1 && thirdNumber <= 0 && secondNumber <= 0))) || (firstNumber < 10 && (fourthNumber <= 0 && thirdNumber <= 0 && secondNumber <= 0)))) {
            return true;
        } else {
            System.out.println("ne delaeca korabl, takih mnogo");
            return false;
        }
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public int getThirdNumber() {
        return thirdNumber;
    }

    public int getFourthNumber() {
        return fourthNumber;
    }

    public boolean isReadyToFight() {
        toCountShips();
        if (fourthNumber == 1 && thirdNumber == 2 && secondNumber == 3 && firstNumber == 4) {
            return true;
        } else {
            return false;
        }

    }
}

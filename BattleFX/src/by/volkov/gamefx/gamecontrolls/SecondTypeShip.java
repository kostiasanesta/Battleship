package by.volkov.gamefx.gamecontrolls;


import java.util.ArrayList;
import java.util.List;

public class SecondTypeShip implements ITargetCell {
    private List<GameCell> twoDeckShip;
    private GameCellType type;
    private String shipIndex;

    public SecondTypeShip(GameCell firstCell, GameCell secondCell) {
        twoDeckShip = new ArrayList<>();
        type = GameCellType.TWO_DECK_SHIP;
        shipIndex = Integer.toString(firstCell.getIndex()) + Integer.toString(secondCell.getIndex())+String.valueOf(type);
        twoDeckShip.add(0,firstCell);
        twoDeckShip.add(1,secondCell);
        for (GameCell cell : twoDeckShip) {
            cell.setShipIndex(shipIndex);
            cell.setType(type);
        }
    }

    @Override
    public List<GameCell> getShip() {
        return twoDeckShip;
    }

    @Override
    public String getShipIndex() {
        return shipIndex;
    }


    public void setCellType(GameCell cell) {
        cell.setType(GameCellType.TWO_DECK_SHIP);
    }

    @Override
    public GameCellType getType() {
        return type;
    }


}

package by.volkov.gamefx;

import by.volkov.gamefx.core.ScreenManager;
import javafx.application.Application;
import javafx.stage.Stage;


public class MyApplication extends Application {
    public static void main(String[] args){
        MyApplication application = new MyApplication();
        application.runTheApp();
    }

    private void runTheApp() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ScreenManager.getInstance().setWindow(primaryStage);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_LOGIN_SCENE);
        ScreenManager.getInstance().show();

    }
}

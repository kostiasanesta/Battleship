package by.volkov.gamefx.core;


import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScreenManager {

    public static final int SCREEN_LOGIN_SCENE = 0;
    public static final int SCREEN_MAIN_SCENE = 2;
    public static final int SCREEN_GAME_SCENE = 3;
    public static final int SCREEN_FLEET_SCENE = 1;
    public static final int SCREEN_OVER_SCENE = 4;
    public static final int SCREEN_DUEL_SCENE = 5;
    public static final int SCREEN_WAITING_SCENE = 6;

    private List<Scene> modalityScenes;
    private Stage modalityStage;
    private List<Scene> scenes;
    private Stage window;
    private static volatile ScreenManager instance = null;


    private ScreenManager() {
        scenes = new ArrayList<>();
        modalityScenes = new ArrayList<>();
    }

    public static ScreenManager getInstance() {
        if (instance == null) {
            synchronized (ScreenManager.class) {
                if (instance == null) {
                    instance = new ScreenManager();
                }
            }
        }
        return instance;
    }

    public void setWindow(Stage window) {
        this.window = window;
    }

    public void switchScene(final int sceneIndex) {
        try {
            Scene scene = scenes.get(sceneIndex);
        } catch (Exception e) {
            inintScene(sceneIndex);
        }
        Platform.runLater(() -> {
            window.setScene(scenes.get(sceneIndex));
        });
    }

    private void inintScene(int sceneIndex) {
        Scene scene = null;
        Pane pane = null;
        switch (sceneIndex) {
            case SCREEN_LOGIN_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/SceneLogin.fxml"));
                    scene = new Scene(pane, 640, 480);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SCREEN_MAIN_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/MainScene.fxml"));
                    scene = new Scene(pane, 640, 480);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SCREEN_GAME_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/GameScene.fxml"));
                    scene = new Scene(pane, 640, 480);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SCREEN_FLEET_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/FleetScene.fxml"));
                    scene = new Scene(pane, 640, 480);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SCREEN_OVER_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/WinScene.fxml"));
                    scene = new Scene(pane, 640, 480);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
        scenes.add(sceneIndex, scene);
    }

    public void show() {
        if (window != null) {
            window.show();
        }
    }

    public List<Scene> getScenes() {
        return scenes;
    }

    public Stage getWindow() {
        return window;
    }

    public void createModalityScreen(int sceneIndex) {
        Platform.runLater(() -> {
            modalityStage = new Stage();
            modalityStage.initModality(Modality.APPLICATION_MODAL);
            modalityStage.initOwner(window);
            showModalityScreen(sceneIndex);
        });
    }

    public void showModalityScreen(int sceneIndex) {

        Scene scene = null;
        Pane pane = null;
        switch (sceneIndex) {
            case SCREEN_DUEL_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/DuelScreen.fxml"));
                    scene = new Scene(pane, 300, 200);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SCREEN_WAITING_SCENE:
                try {
                    pane = (Pane) FXMLLoader.load(getClass().getResource("../scenes/WaitingScene.fxml"));
                    scene = new Scene(pane, 300, 200);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
            modalityStage.setScene(scene);
            modalityStage.showAndWait();

        }
        public void closeModalityStage(){
        modalityStage.close();
        }

    public void setScenes(List<Scene> scenes) {
        this.scenes = scenes;
    }
}



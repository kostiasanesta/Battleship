package by.volkov.gamefx.core;


import by.seabattle.messaging.interfaces.IMessageDispatcher;
import by.seabattle.messaging.units.OnlineUser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.List;

public class ThreadManager {
    private OnlineUser enemyUser;
    private String recipientId;
    private OnlineUser playerUser;
    private ObservableList<OnlineUser> onlineUsers;
    private ClientThread clientThread;
    private static volatile ThreadManager instance = null;

    private ThreadManager() {
        onlineUsers = FXCollections.observableArrayList();
    }

    public static ThreadManager getInstance() {
        if (instance == null) {
            synchronized (ThreadManager.class) {
                if (instance == null) {
                    instance = new ThreadManager();
                }
            }
        }
        return instance;
    }

    public void initClientThread(String name, String hostIp, int port) throws IOException {
        clientThread = new ClientThread(name, hostIp, port);
        clientThread.start();
    }

    public void setClientThreadDelegate(IMessageDispatcher delegate) {
        if (clientThread != null) {
            clientThread.setDelegate(delegate);
        }
    }

    public ClientThread getClientThread() {
        return clientThread;
    }

    public ObservableList<OnlineUser> getOnlineUsers() {
        return onlineUsers;
    }

    public void createOnlineUsersList(List<OnlineUser> onlineUsers) {
        this.onlineUsers = FXCollections.observableArrayList();
        for (OnlineUser user: onlineUsers){
            if(!user.isBusy() && getClientThread().getClientName()!=user.getName()){
                this.onlineUsers.add(user);
            }
        }
    }
    public void savePlayerOnlineUser(String userName){
        for (OnlineUser user: onlineUsers){
            if(user.getName()==userName){
                playerUser = user;
                return;
            }
        }
    }

    public OnlineUser getPlayerUser() {
        return playerUser;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public OnlineUser getEnemyUser() {
        return enemyUser;
    }

    public void setEnemyUser(OnlineUser enemyUser) {
        this.enemyUser = enemyUser;
    }
}

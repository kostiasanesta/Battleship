package by.volkov.gamefx.core;


import by.seabattle.messaging.interfaces.IMessageDispatcher;
import by.seabattle.messaging.messages.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientThread extends Thread {


    private boolean isRuning;
    private Socket socket;
    private String clientName;
    private String clientId;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private IMessageDispatcher delegate;

    public ClientThread(String clientName, String serverIp, int serverPort) throws IOException {
        socket = new Socket(InetAddress.getByName(serverIp),serverPort);
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());
        this.clientName = clientName;
    }

    public void sendMessage(Message message) throws IOException {
        if (outputStream!=null) {
            synchronized (outputStream){
                outputStream.writeObject(message);
            }
        }
    }

    @Override
    public void run() {
        isRuning = true;
        while (isRuning){
            try {
                Message message = (Message) inputStream.readObject();
                if(delegate!=null){
                    delegate.onMessageReceived(message);
                }
                Thread.sleep(100);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                isRuning = false;
            }
        }
        try {
            inputStream.close();
            outputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isRuning() {
        return isRuning;
    }

    public void setRuning(boolean runing) {
        isRuning = runing;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public IMessageDispatcher getDelegate() {
        return delegate;
    }

    public void setDelegate(IMessageDispatcher delegate) {
        this.delegate = delegate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}

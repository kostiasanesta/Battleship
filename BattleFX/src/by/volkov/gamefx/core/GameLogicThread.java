package by.volkov.gamefx.core;


import by.volkov.gamefx.gamecontrolls.*;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameLogicThread extends Thread {
    private ITargetCell shipTarget;
    private GameCell shotTarget;
    private Fleet playerFleet;
    private boolean isWinner;
    private boolean isRuning;
    private GameCell[][] myCells;
    private GameCell[][] enemyCells;
    private List<GameCell> topCells;
    private List<GameCell> botCells;
    private List<GameCell> leftCells;
    private List<GameCell> rightCells;
    private List<GameCell> workingCells;
    private List<GameCell> commonCells;
    private List<GameCell> cellsToSend;
    private boolean yourTurn;
    private boolean canFire;
    private double side;
    private double shotNumber;
    private double enemyShotNumber;
    private double hitNumber;
    private double enemyHitNumber;

    public GameLogicThread() {
        myCells = new GameCell[10][10];
        enemyCells = new GameCell[10][10];
        workingCells = new ArrayList<>();
        topCells = new ArrayList<>();
        botCells = new ArrayList<>();
        leftCells = new ArrayList<>();
        rightCells = new ArrayList<>();
        commonCells = new ArrayList<>();
        playerFleet = new Fleet();
        cellsToSend = new ArrayList<>();
    }

    public void initCells(double side) {
        this.side = side;
        int index = 0;
        for (int i = 0; i < myCells.length; i++) {
            for (int j = 0; j < myCells[i].length; j++) {
                GameCell cell = new GameCell(side);
                workingCells.add(index, cell);
                myCells[i][j] = cell;
                myCells[i][j].setIndex(index);
                myCells[i][j].setPoint2D(new Point2D(j, i));
                enemyCells[i][j] = new GameCell(side);
                enemyCells[i][j].setIndex(index);
                enemyCells[i][j].setPoint2D(new Point2D(j, i));
                if (index > 0 && index < 9) {
                    topCells.add(cell);
                } else if (index > 90 && index < 99) {
                    botCells.add(cell);
                } else if (index > 0 && index < 90 && index % 10 == 0) {
                    leftCells.add(cell);
                } else if (index > 9 && index < 99 && index % 10 == 9) {
                    rightCells.add(cell);
                } else if (index != 0 && index != 9 && index != 90 && index != 99) {
                    commonCells.add(cell);
                }
                index++;
            }
        }
        System.out.println(index);
    }

    public GameCell[][] getMyCells() {
        return myCells;
    }

    public GameCell[][] getEnemyCells() {
        return enemyCells;
    }

    public void proccesPlayerCanvasClick(Point2D point, double side) {
        GameCell cell = myCells[(int) ((point.getY() - side) / side)][(int) ((point.getX() - side) / side)];
        playerFleet.toCountShips();
        if (cell.isReadyToBuild() && cell.getStatus() != GameCellStatus.SHIP) {
            if (processWithAngleIndexes(cell)) {

            } else if (processWithTopIndexes(cell)) {

            } else if (processWithBotIndexes(cell)) {

            } else if (processWithRightIndexes(cell)) {

            } else if (processWithLeftIndexes(cell)) {

            } else if (proccesWithCommonIndexes(cell)) {

            }
        }
    }

    private boolean proccesWithCommonIndexes(GameCell cell) {
        for (GameCell commonCell : commonCells) {
            if (commonCell.getIndex() == cell.getIndex()) {
                if ((workingCells.get(cell.getIndex() - 10).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() + 10).getStatus() == GameCellStatus.SHIP) ||
                        (workingCells.get(cell.getIndex() + 1).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() - 1).getStatus() == GameCellStatus.SHIP)) {
                    cell.setReadyToBuild(false);
                    return false;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 10))) {
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 1))) {
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 10))) {
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 1))) {
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (workingCells.get(cell.getIndex() - 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 10).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() - 10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                    if (playerFleet.getFourthNumber() == 1 && (workingCells.get(cell.getIndex() - 10).getType() == GameCellType.THIRD_DECK_SHIP
                            || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() + 10).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.THIRD_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.TWO_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() - 10).getType() == GameCellType.TWO_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.ONE_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() - 10).getType() == GameCellType.ONE_DECK_SHIP)) {
                        return false;
                    } else if (createNewTypeOfTheShip(cell)) {
                        workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean processWithLeftIndexes(GameCell cell) {
        for (GameCell topCell : leftCells) {
            if (topCell.getIndex() == cell.getIndex()) {
                if ((workingCells.get(cell.getIndex() - 10).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() + 10).getStatus() == GameCellStatus.SHIP)) {
                    cell.setReadyToBuild(false);
                    return false;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 10))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 1))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 10))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (workingCells.get(cell.getIndex() - 10).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                    if (playerFleet.getFourthNumber() == 1 && (workingCells.get(cell.getIndex() - 10).getType() == GameCellType.THIRD_DECK_SHIP
                            || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() + 10).getType() == GameCellType.THIRD_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (
                            workingCells.get(cell.getIndex() - 10).getType() == GameCellType.TWO_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.TWO_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                            workingCells.get(cell.getIndex() - 10).getType() == GameCellType.ONE_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.ONE_DECK_SHIP)) {
                        return false;
                    } else if (createNewTypeOfTheShip(cell)) {
                        workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean processWithRightIndexes(GameCell cell) {
        for (GameCell topCell : rightCells) {
            if (topCell.getIndex() == cell.getIndex()) {
                if ((workingCells.get(cell.getIndex() - 10).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() + 10).getStatus() == GameCellStatus.SHIP)) {
                    cell.setReadyToBuild(false);
                    return false;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 10))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 1))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 10))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (workingCells.get(cell.getIndex() - 10).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() - 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                    if (playerFleet.getFourthNumber() == 1 && (workingCells.get(cell.getIndex() - 10).getType() == GameCellType.THIRD_DECK_SHIP
                            || workingCells.get(cell.getIndex() - 1).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() + 10).getType() == GameCellType.THIRD_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (
                            workingCells.get(cell.getIndex() - 10).getType() == GameCellType.TWO_DECK_SHIP
                                    || workingCells.get(cell.getIndex() - 1).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.TWO_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                            workingCells.get(cell.getIndex() - 10).getType() == GameCellType.ONE_DECK_SHIP
                                    || workingCells.get(cell.getIndex() - 1).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.ONE_DECK_SHIP)) {
                        return false;
                    } else if (createNewTypeOfTheShip(cell)) {
                        workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean processWithBotIndexes(GameCell cell) {
        for (GameCell topCell : botCells) {
            if (topCell.getIndex() == cell.getIndex()) {
                if ((workingCells.get(cell.getIndex() - 1).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() + 1).getStatus() == GameCellStatus.SHIP)) {
                    cell.setReadyToBuild(false);
                    return false;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 1))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 1))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 10))) {
                    workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                    return true;
                } else if (workingCells.get(cell.getIndex() - 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() - 10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                    if (playerFleet.getFourthNumber() == 1 && (workingCells.get(cell.getIndex() - 1).getType() == GameCellType.THIRD_DECK_SHIP
                            || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() - 10).getType() == GameCellType.THIRD_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.TWO_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() - 10).getType() == GameCellType.TWO_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.ONE_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() - 10).getType() == GameCellType.ONE_DECK_SHIP)) {
                        return false;
                    } else if (createNewTypeOfTheShip(cell)) {
                        workingCells.get(cell.getIndex() - 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() - 11).setReadyToBuild(false);
                        return true;
                    }
                }
            }
        }
        return false;

    }

    private boolean processWithTopIndexes(GameCell cell) {
        for (GameCell topCell : topCells) {
            if (topCell.getIndex() == cell.getIndex()) {
                if ((workingCells.get(cell.getIndex() - 1).getStatus() == GameCellStatus.SHIP && workingCells.get(cell.getIndex() + 1).getStatus() == GameCellStatus.SHIP)) {
                    cell.setReadyToBuild(false);
                    return false;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() - 1))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 1))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (createNewTypeOfTheShip(cell, workingCells.get(cell.getIndex() + 10))) {
                    workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                    workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                    return true;
                } else if (workingCells.get(cell.getIndex() - 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 1).getType() != GameCellType.FOURTH_DECK_SHIP &&
                        workingCells.get(cell.getIndex() + 10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                    if (playerFleet.getFourthNumber() == 1 && (workingCells.get(cell.getIndex() - 1).getType() == GameCellType.THIRD_DECK_SHIP
                            || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.THIRD_DECK_SHIP ||
                            workingCells.get(cell.getIndex() + 10).getType() == GameCellType.THIRD_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.TWO_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.TWO_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.TWO_DECK_SHIP)) {
                        return false;
                    } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                            workingCells.get(cell.getIndex() - 1).getType() == GameCellType.ONE_DECK_SHIP
                                    || workingCells.get(cell.getIndex() + 1).getType() == GameCellType.ONE_DECK_SHIP ||
                                    workingCells.get(cell.getIndex() + 10).getType() == GameCellType.ONE_DECK_SHIP)) {
                        return false;
                    } else if (createNewTypeOfTheShip(cell)) {
                        workingCells.get(cell.getIndex() + 9).setReadyToBuild(false);
                        workingCells.get(cell.getIndex() + 11).setReadyToBuild(false);
                        return true;
                    }
                }
            }
        }
        return false;

    }

    @Override
    public void run() {
        isRuning = true;
        while (isRuning) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                isRuning = false;
            }


        }
    }


    private boolean processWithAngleIndexes(GameCell cell) {
        if (cell.getIndex() == 0) {
            if (createNewTypeOfTheShip(cell, workingCells.get(1))) {
                workingCells.get(11).setReadyToBuild(false);
                return true;
            } else if (createNewTypeOfTheShip(cell, workingCells.get(10))) {
                workingCells.get(11).setReadyToBuild(false);
                return true;
            } else if (workingCells.get(1).getType() != GameCellType.FOURTH_DECK_SHIP && workingCells.get(10).getType() != GameCellType.FOURTH_DECK_SHIP) {
                if (playerFleet.getFourthNumber() == 1 && (workingCells.get(1).getType() == GameCellType.THIRD_DECK_SHIP
                        || workingCells.get(10).getType() == GameCellType.THIRD_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (workingCells.get(1).getType() == GameCellType.TWO_DECK_SHIP ||
                        workingCells.get(10).getType() == GameCellType.TWO_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                        workingCells.get(1).getType() == GameCellType.ONE_DECK_SHIP ||
                                workingCells.get(10).getType() == GameCellType.ONE_DECK_SHIP)) {
                    return false;
                } else if (createNewTypeOfTheShip(cell)) {
                    workingCells.get(11).setReadyToBuild(false);
                    return true;
                }
            }
        } else if (cell.getIndex() == 9) {
            if (createNewTypeOfTheShip(cell, workingCells.get(8))) {
                workingCells.get(18).setReadyToBuild(false);
                return true;
            } else if (createNewTypeOfTheShip(cell, workingCells.get(19))) {
                workingCells.get(18).setReadyToBuild(false);
                return true;
            } else if (workingCells.get(8).getType() != GameCellType.FOURTH_DECK_SHIP && workingCells.get(19).getType() != GameCellType.FOURTH_DECK_SHIP) {
                if (playerFleet.getFourthNumber() == 1 && (
                        workingCells.get(8).getType() == GameCellType.THIRD_DECK_SHIP
                                || workingCells.get(19).getType() == GameCellType.THIRD_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (workingCells.get(8).getType() == GameCellType.TWO_DECK_SHIP ||
                        workingCells.get(19).getType() == GameCellType.TWO_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                        workingCells.get(8).getType() == GameCellType.ONE_DECK_SHIP ||
                                workingCells.get(19).getType() == GameCellType.ONE_DECK_SHIP)) {
                    return false;
                } else if (createNewTypeOfTheShip(cell)) {
                    workingCells.get(18).setReadyToBuild(false);
                    return true;
                }
            }
        } else if (cell.getIndex() == 99) {
            if (createNewTypeOfTheShip(cell, workingCells.get(89))) {
                workingCells.get(88).setReadyToBuild(false);
                return true;
            } else if (createNewTypeOfTheShip(cell, workingCells.get(98))) {
                workingCells.get(88).setReadyToBuild(false);
                return true;
            } else if (workingCells.get(89).getType() != GameCellType.FOURTH_DECK_SHIP && workingCells.get(98).getType() != GameCellType.FOURTH_DECK_SHIP) {
                if (playerFleet.getFourthNumber() == 1 && (workingCells.get(89).getType() == GameCellType.THIRD_DECK_SHIP
                        || workingCells.get(98).getType() == GameCellType.THIRD_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (workingCells.get(89).getType() == GameCellType.TWO_DECK_SHIP ||
                        workingCells.get(98).getType() == GameCellType.TWO_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                        workingCells.get(89).getType() == GameCellType.ONE_DECK_SHIP ||
                                workingCells.get(98).getType() == GameCellType.ONE_DECK_SHIP)) {
                    return false;
                } else if (createNewTypeOfTheShip(cell)) {
                    workingCells.get(88).setReadyToBuild(false);
                    return true;
                }
            }
        } else if (cell.getIndex() == 90) {
            if (createNewTypeOfTheShip(cell, workingCells.get(91))) {
                workingCells.get(81).setReadyToBuild(false);
                return true;
            } else if (createNewTypeOfTheShip(cell, workingCells.get(80))) {
                workingCells.get(81).setReadyToBuild(false);
                return true;
            } else if (workingCells.get(91).getType() != GameCellType.FOURTH_DECK_SHIP && workingCells.get(80).getType() != GameCellType.FOURTH_DECK_SHIP) {
                if (playerFleet.getFourthNumber() == 1 && (workingCells.get(91).getType() == GameCellType.THIRD_DECK_SHIP
                        || workingCells.get(80).getType() == GameCellType.THIRD_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && (workingCells.get(80).getType() == GameCellType.TWO_DECK_SHIP ||
                        workingCells.get(91).getType() == GameCellType.TWO_DECK_SHIP)) {
                    return false;
                } else if (playerFleet.getFourthNumber() == 1 && playerFleet.getThirdNumber() == 2 && playerFleet.getSecondNumber() == 3 && (
                        workingCells.get(80).getType() == GameCellType.ONE_DECK_SHIP ||
                                workingCells.get(91).getType() == GameCellType.ONE_DECK_SHIP)) {
                    return false;
                } else if (createNewTypeOfTheShip(cell)) {
                    workingCells.get(81).setReadyToBuild(false);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean createNewTypeOfTheShip(GameCell ourCell) {
        if (playerFleet.testingToAdd(GameCellType.ONE_DECK_SHIP)) {
            playerFleet.addToFleet(new FirstTypeShip(ourCell));
            ourCell.setStatus(GameCellStatus.SHIP);
            ourCell.setReadyToBuild(false);
            return true;
        } else {
            return false;
        }

    }

    private boolean createNewTypeOfTheShip(GameCell ourCell, GameCell neigborCell) {
        String indexToRemove = neigborCell.getShipIndex();
        System.out.println("neig index " + neigborCell.getIndex() + " neig ship index " + neigborCell.getShipIndex());
        GameCell targetSecond = null;
        GameCell targetThird = null;
        GameCell targetFourth = null;
        switch (neigborCell.getType()) {
            case ONE_DECK_SHIP:
                for (ITargetCell iTargetCell : playerFleet.getFleet()) {
                    if (iTargetCell.getShipIndex() == indexToRemove) {
                        targetSecond = iTargetCell.getShip().get(0);
                    }
                }
                if (playerFleet.testingToAdd(GameCellType.TWO_DECK_SHIP)) {
                    playerFleet.addToFleet(new SecondTypeShip(ourCell, targetSecond));
                    processToRemoveShip(indexToRemove);
                    ourCell.setStatus(GameCellStatus.SHIP);
                    ourCell.setReadyToBuild(false);
                    return true;
                } else {
                    return false;
                }
            case TWO_DECK_SHIP:
                for (ITargetCell iTargetCell : playerFleet.getFleet()) {
                    if (iTargetCell.getShipIndex() == indexToRemove) {
                        targetSecond = iTargetCell.getShip().get(0);
                        targetThird = iTargetCell.getShip().get(1);
                    }
                }
                if (playerFleet.testingToAdd(GameCellType.THIRD_DECK_SHIP)) {
                    playerFleet.addToFleet(new ThirdTypeShip(ourCell, targetSecond, targetThird));
                    processToRemoveShip(indexToRemove);
                    ourCell.setStatus(GameCellStatus.SHIP);
                    ourCell.setReadyToBuild(false);
                    return true;
                } else {
                    return false;
                }

            case THIRD_DECK_SHIP:
                for (ITargetCell iTargetCell : playerFleet.getFleet()) {
                    if (iTargetCell.getShipIndex() == indexToRemove) {
                        targetSecond = iTargetCell.getShip().get(0);
                        targetThird = iTargetCell.getShip().get(1);
                        targetFourth = iTargetCell.getShip().get(2);
                    }
                }
                if (playerFleet.testingToAdd(GameCellType.FOURTH_DECK_SHIP)) {
                    playerFleet.addToFleet(new FourthTypeShip(ourCell, targetSecond, targetThird, targetFourth));
                    processToRemoveShip(indexToRemove);
                    ourCell.setStatus(GameCellStatus.SHIP);
                    ourCell.setReadyToBuild(false);
                    return true;
                } else {
                    return false;
                }

            case WATER:
                return false;
            case FOURTH_DECK_SHIP:
                ourCell.setReadyToBuild(false);
                return false;
            default:
                return false;

        }
    }

    private void processToRemoveShip(String indexToRemove) {
        ITargetCell targetToRemove = null;
        for (ITargetCell iTargetCell : playerFleet.getFleet()) {
            if (iTargetCell.getShipIndex() == indexToRemove) {
                System.out.println(iTargetCell.getShipIndex());
                iTargetCell.getShip().clear();
                targetToRemove = iTargetCell;
            }
        }
        playerFleet.getFleet().remove(targetToRemove);
    }

    public void autoDraw() {
        Random random = new Random();
        int i = 0;
        while (playerFleet.getFourthNumber() != 1 || playerFleet.getThirdNumber() != 2 || playerFleet.getSecondNumber() != 3 || playerFleet.getFirstNumber() != 4) {
            if (i < 10000) {
                i++;
                GameCell cell = workingCells.get(random.nextInt(100));
                if (cell.isReadyToBuild() && cell.getStatus() != GameCellStatus.SHIP) {
                    System.out.println("index " + cell.getIndex());
                    System.out.println("SHip type " + cell.getType());
                    if (processWithAngleIndexes(cell)) {

                    } else if (processWithTopIndexes(cell)) {

                    } else if (processWithBotIndexes(cell)) {

                    } else if (processWithRightIndexes(cell)) {

                    } else if (processWithLeftIndexes(cell)) {

                    } else if (proccesWithCommonIndexes(cell)) {

                    }
                }
            } else {
                clear(side);
                autoDraw();
            }
        }
    }

    public void clear(double side) {
        for (ITargetCell targetCell : playerFleet.getFleet()) {
            targetCell.getShip().clear();
        }
        playerFleet.getFleet().clear();
        playerFleet.toCountShips();
        initCells(side);
    }

    public Fleet getPlayerFleet() {
        return playerFleet;
    }

    public int processOfFire(Point2D point, double v) {
        if (isYourTurn()) {
            if (canFire) {
                GameCell cell = enemyCells[(int) ((point.getY() - side) / side)][(int) ((point.getX() - side) / side)];
                if (cell.isCanBeFired()) {
                    canFire = false;
                    shotNumber++;
                    return cell.getIndex();
                }
            }
        }
        return 1000;
    }

    public boolean isYourTurn() {
        return yourTurn;
    }

    public void setYourTurn(boolean yourTurn) {
        this.yourTurn = yourTurn;
    }

    public void checkTheHit(int cellIndex) {
        enemyShotNumber++;
        shotTarget = workingCells.get(cellIndex);
        if (shotTarget.getStatus() == GameCellStatus.SHIP) {
            enemyHitNumber++;
            shotTarget.setStatus(GameCellStatus.SHIP_DAMAGED);
            shotTarget.setDamaged(true);
            if (checkForDestroy()) {
                consequencesOfDestruction();
            }

        } else {
            shotTarget.setStatus(GameCellStatus.MISS);
        }
        shotTarget.setCanBeFired(false);
    }

    private void consequencesOfDestruction() {
        GameCell maxPosition = null;
        GameCell minPosition = null;
        switch (shipTarget.getType()) {
            case ONE_DECK_SHIP:
                processOfDestroyFirstTypeShip(shipTarget.getShip().get(0));
                break;
            case TWO_DECK_SHIP:
                if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                    maxPosition = shipTarget.getShip().get(0);
                    minPosition = shipTarget.getShip().get(1);
                } else {
                    maxPosition = shipTarget.getShip().get(1);
                    minPosition = shipTarget.getShip().get(0);
                }
                cellsToSend.add(maxPosition);
                cellsToSend.add(minPosition);
                if (checkThePosition(shipTarget)) {
                    processWithVerticalMaxPosition(maxPosition);
                    processWithVerticalMinPosition(minPosition);
                } else {
                    processWithHorizontalMaxPosition(maxPosition);
                    processWithHorizontalMinPosition(minPosition);
                }
                break;
            case THIRD_DECK_SHIP:
                if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex() &&
                        shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                    if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                        maxPosition = shipTarget.getShip().get(0);
                        minPosition = shipTarget.getShip().get(1);
                    } else {
                        maxPosition = shipTarget.getShip().get(0);
                        minPosition = shipTarget.getShip().get(2);
                    }
                } else if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(0).getIndex() &&
                        shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                    if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                        maxPosition = shipTarget.getShip().get(1);
                        minPosition = shipTarget.getShip().get(0);
                    } else {
                        maxPosition = shipTarget.getShip().get(1);
                        minPosition = shipTarget.getShip().get(2);
                    }
                } else if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex() &&
                        shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                    if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                        maxPosition = shipTarget.getShip().get(2);
                        minPosition = shipTarget.getShip().get(1);
                    } else {
                        maxPosition = shipTarget.getShip().get(2);
                        minPosition = shipTarget.getShip().get(0);
                    }
                }
                for (GameCell cell : shipTarget.getShip()) {
                    cellsToSend.add(cell);
                }
                if (checkThePosition(shipTarget)) {
                    processWithVerticalMaxPosition(maxPosition);
                    processWithVerticalMinPosition(minPosition);
                } else {
                    processWithHorizontalMaxPosition(maxPosition);
                    processWithHorizontalMinPosition(minPosition);
                }
                break;
            case FOURTH_DECK_SHIP:
                for (GameCell cell : shipTarget.getShip()) {
                    cellsToSend.add(cell);
                }
                if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex()
                        && shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(2).getIndex()
                        && shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                    if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    } else if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                        if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(1);
                        }
                    }else  if (shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(1).getIndex() &&
                            shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(1);
                        } else {
                            maxPosition = shipTarget.getShip().get(0);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    }

                }else if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(0).getIndex()
                        && shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(2).getIndex()
                        && shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                    if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    } else if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                        if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(0);
                        }
                    }else  if (shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(0).getIndex() &&
                            shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(0);
                        } else {
                            maxPosition = shipTarget.getShip().get(1);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    }

                }else if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()
                        && shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()
                        && shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                    if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                        if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(0);
                        }
                    } else if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(3).getIndex() &&
                            shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                        if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(3).getIndex()) {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(3);
                        } else {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(1);
                        }
                    }else  if (shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(1).getIndex() &&
                            shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                        if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(1);
                        } else {
                            maxPosition = shipTarget.getShip().get(2);
                            minPosition = shipTarget.getShip().get(0);
                        }
                    }

                }else if (shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(1).getIndex()
                        && shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(2).getIndex()
                        && shipTarget.getShip().get(3).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                    if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(0).getIndex() &&
                            shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(0);
                        } else {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    } else if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(0).getIndex() &&
                            shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                        if (shipTarget.getShip().get(1).getIndex() > shipTarget.getShip().get(0).getIndex()) {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(0);
                        } else {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(1);
                        }
                    }else  if (shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(1).getIndex() &&
                            shipTarget.getShip().get(0).getIndex() > shipTarget.getShip().get(2).getIndex()) {
                        if (shipTarget.getShip().get(2).getIndex() > shipTarget.getShip().get(1).getIndex()) {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(1);
                        } else {
                            maxPosition = shipTarget.getShip().get(3);
                            minPosition = shipTarget.getShip().get(2);
                        }
                    }
                }
                if (checkThePosition(shipTarget)) {
                    processWithVerticalMaxPosition(maxPosition);
                    processWithVerticalMinPosition(minPosition);
                } else {
                    processWithHorizontalMaxPosition(maxPosition);
                    processWithHorizontalMinPosition(minPosition);
                }
             break;
        }
        playerFleet.getFleet().remove(shipTarget);
    }

    private void processWithHorizontalMaxPosition(GameCell maxPosition) {
        if (maxPosition.getIndex() == 9) {
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
        } else if (maxPosition.getIndex() == 99) {
            workingCells.get(maxPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        } else if ((maxPosition.getIndex() - 9) % 10 == 0) {
            workingCells.get(maxPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
        } else if (maxPosition.getIndex() > 90) {
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        } else if (maxPosition.getIndex() < 9) {
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
        } else {
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        }
    }

    private void processWithHorizontalMinPosition(GameCell minPosition) {
        if (minPosition.getIndex() == 0) {
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 10));
        } else if (minPosition.getIndex() == 90) {
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
        } else if (minPosition.getIndex() < 9) {
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        } else if (minPosition.getIndex() % 10 == 0) {
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 10));
        } else if (minPosition.getIndex() > 90) {
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
        } else {
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        }
    }

    private void processWithVerticalMinPosition(GameCell minPosition) {
        if (minPosition.getIndex() == 0) {
            workingCells.get(minPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        } else if (minPosition.getIndex() == 9) {
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
        } else if (minPosition.getIndex() < 9) {
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        } else if ((minPosition.getIndex() - 9) % 10 == 0) {
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
        } else if (minPosition.getIndex() % 10 == 0) {
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        } else {
            workingCells.get(minPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(minPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 11));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 10));
            cellsToSend.add(workingCells.get(minPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(minPosition.getIndex() + 11));
        }
    }

    private void processWithVerticalMaxPosition(GameCell maxPosition) {
        if (maxPosition.getIndex() == 90) {
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
        } else if (maxPosition.getIndex() == 99) {
            workingCells.get(maxPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        } else if ((maxPosition.getIndex() - 9) % 10 == 0) {
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        } else if (maxPosition.getIndex() > 90) {
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        } else if (maxPosition.getIndex() % 10 == 0) {
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
        } else {
            workingCells.get(maxPosition.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(maxPosition.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 11));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 10));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() + 9));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 1));
            cellsToSend.add(workingCells.get(maxPosition.getIndex() - 11));
        }
    }

    private boolean checkThePosition(ITargetCell target) {
        if ((target.getShip().get(0).getIndex() - target.getShip().get(1).getIndex()) % 10 == 0) {
            return true;
        }
        return false;
    }

    private void processOfDestroyFirstTypeShip(GameCell cell) {
        if (cell.getIndex() == 0) {
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(workingCells.get(cell.getIndex() + 11));
            cellsToSend.add(cell);
        } else if (cell.getIndex() == 9) {
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(workingCells.get(cell.getIndex() + 9));
            cellsToSend.add(cell);
        } else if (cell.getIndex() == 99) {
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 11));
            cellsToSend.add(cell);
        } else if (cell.getIndex() == 90) {
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 9));
            cellsToSend.add(cell);
        } else if (cell.getIndex() > 0 && cell.getIndex() < 9) {
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 9));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(workingCells.get(cell.getIndex() + 11));
            cellsToSend.add(cell);
        } else if ((cell.getIndex() - 9) % 10 == 0 && cell.getIndex() != 9 && cell.getIndex() != 99) {
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 11));
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 9));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(cell);
        } else if (cell.getIndex() > 90 && cell.getIndex() < 99) {
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 9));
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 11));
            cellsToSend.add(cell);
        } else if (cell.getIndex() % 10 == 0 && cell.getIndex() != 0 && cell.getIndex() != 90) {
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 9).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 11).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 9));
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(workingCells.get(cell.getIndex() + 11));
            cellsToSend.add(cell);
        } else {
            workingCells.get(cell.getIndex() + 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 11).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() + 9).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 1).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 11).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 10).setStatus(GameCellStatus.MISS);
            workingCells.get(cell.getIndex() - 9).setStatus(GameCellStatus.MISS);
            cellsToSend.add(workingCells.get(cell.getIndex() + 1));
            cellsToSend.add(workingCells.get(cell.getIndex() + 11));
            cellsToSend.add(workingCells.get(cell.getIndex() + 10));
            cellsToSend.add(workingCells.get(cell.getIndex() + 9));
            cellsToSend.add(workingCells.get(cell.getIndex() - 1));
            cellsToSend.add(workingCells.get(cell.getIndex() - 11));
            cellsToSend.add(workingCells.get(cell.getIndex() - 10));
            cellsToSend.add(workingCells.get(cell.getIndex() - 9));
            cellsToSend.add(cell);
        }
    }


    private boolean checkForDestroy() {
        for (ITargetCell iTargetCell : playerFleet.getFleet()) {
            if (iTargetCell.getShipIndex() == shotTarget.getShipIndex()) {
                switch (iTargetCell.getType()) {
                    case ONE_DECK_SHIP:
                        iTargetCell.getShip().get(0).setStatus(GameCellStatus.SHIP_DESTROYED);
                        shipTarget = iTargetCell;
                        return true;
                    case TWO_DECK_SHIP:
                        if (iTargetCell.getShip().get(0).isDamaged() && iTargetCell.getShip().get(1).isDamaged()) {
                            for (GameCell cell : iTargetCell.getShip()) {
                                cell.setStatus(GameCellStatus.SHIP_DESTROYED);
                            }
                            shipTarget = iTargetCell;
                            return true;
                        }
                        return false;
                    case THIRD_DECK_SHIP:
                        if (iTargetCell.getShip().get(0).isDamaged() && iTargetCell.getShip().get(1).isDamaged() && iTargetCell.getShip().get(2).isDamaged()) {
                            for (GameCell cell : iTargetCell.getShip()) {
                                cell.setStatus(GameCellStatus.SHIP_DESTROYED);
                            }
                            shipTarget = iTargetCell;
                            return true;
                        }
                        return false;
                    case FOURTH_DECK_SHIP:
                        if (iTargetCell.getShip().get(0).isDamaged() && iTargetCell.getShip().get(1).isDamaged() &&
                                iTargetCell.getShip().get(2).isDamaged() && iTargetCell.getShip().get(3).isDamaged()) {
                            for (GameCell cell : iTargetCell.getShip()) {
                                cell.setStatus(GameCellStatus.SHIP_DESTROYED);
                            }
                            shipTarget = iTargetCell;
                            return true;
                        }
                        return false;
                }
            }
        }
        return false;
    }

    public GameCell getShotTarget() {
        return shotTarget;
    }

    public boolean isCanFire() {
        return canFire;
    }

    public void setCanFire(boolean canFire) {
        this.canFire = canFire;
    }

    public void lookAtTheResultOfShooting(int cellIndex, boolean damaged, boolean destroyed) {
        for (GameCell[] enemyCell : enemyCells) {
            for (GameCell cell : enemyCell) {
                if (cell.getIndex() == cellIndex) {
                    if (destroyed && damaged) {
                        cell.setStatus(GameCellStatus.SHIP_DESTROYED);
                        cell.setCanBeFired(false);
                        hitNumber++;
                    } else if (destroyed && !(damaged)) {
                        cell.setStatus(GameCellStatus.MISS);
                        cell.setCanBeFired(false);
                    } else if (damaged && !(destroyed)) {
                        cell.setStatus(GameCellStatus.SHIP_DAMAGED);
                        cell.setCanBeFired(false);
                        hitNumber++;
                    } else {
                        cell.setStatus(GameCellStatus.MISS);
                        cell.setCanBeFired(false);
                    }
                }
            }
        }
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner(boolean winner) {
        isWinner = winner;
    }

    public double getShotNumber() {
        return shotNumber;
    }

    public double getEnemyShotNumber() {
        return enemyShotNumber;
    }

    public double getHitNumber() {
        return hitNumber;
    }

    public double getEnemyHitNumber() {
        return enemyHitNumber;
    }

    public List<GameCell> getCellsToSend() {
        return cellsToSend;
    }

    public void setRuning(boolean runing) {
        isRuning = runing;
    }
}

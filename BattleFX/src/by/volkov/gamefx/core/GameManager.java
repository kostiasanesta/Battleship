package by.volkov.gamefx.core;


import by.volkov.gamefx.gamecontrolls.GameCell;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;

public class GameManager {
    private  GameBuilder builder;
    private GameLogicThread gameLogicThread;
    public static volatile GameManager instance = null;

    private GameManager(){
        builder=new GameBuilder();
    }

    public static GameManager getInstance(){
        if(instance == null){
            synchronized (GameManager.class){
                if(instance==null){
                    instance = new GameManager();
                }
            }
        }
        return instance;
    }

    public void initGameLogicThread(){
        gameLogicThread = new GameLogicThread();
        gameLogicThread.start();
    }

    public GameLogicThread getGameLogicThread() {
        return gameLogicThread;
    }

    public void drawExamples(GraphicsContext gc, double side,double coordStart){

    }

    public void initGame( GraphicsContext gC, GameCell[][] cells, double side){
        gameLogicThread.initCells(side);
        builder.drawNumbersAndLetters(gC,side);
        for (GameCell[] gameCells : cells) {
            for (GameCell cell : gameCells) {
                cell.draw(gC);
            }
        }
        }

    public GameBuilder getBuilder() {
        return builder;
    }


}


package by.volkov.gamefx.controllers;


import by.volkov.gamefx.core.GameManager;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.List;

public class WinScreenController extends BaseController {
    @FXML
    private Label winnerLabel;
    @FXML
    private Label playerLbl;
    @FXML
    private Label enemyLbl;
    @FXML
    private Label playerAccuratyLbl;
    @FXML
    private Label enemyAccuratyLbl;


    @FXML
    public void initialize() {
        if (GameManager.getInstance().getGameLogicThread().isWinner()) {
            winnerLabel.setText("You are WINNER!");
        } else {
            winnerLabel.setText("You lose!");
        }
        playerLbl.setText(ThreadManager.getInstance().getPlayerUser().getName());
        enemyLbl.setText(ThreadManager.getInstance().getEnemyUser().getName());
        if (GameManager.getInstance().getGameLogicThread().getEnemyShotNumber() != 0 && GameManager.getInstance().getGameLogicThread().getShotNumber() != 0) {
            enemyAccuratyLbl.setText(((GameManager.getInstance().getGameLogicThread().getEnemyHitNumber() / GameManager.getInstance().getGameLogicThread().getEnemyShotNumber()) * 100) + "% of accuraty");
            playerAccuratyLbl.setText(((GameManager.getInstance().getGameLogicThread().getHitNumber() / GameManager.getInstance().getGameLogicThread().getShotNumber()) * 100) + "% of accuraty");
        } else {
            enemyAccuratyLbl.setVisible(false);
            playerAccuratyLbl.setVisible(false);
        }
        ThreadManager.getInstance().getClientThread().setDelegate(this);


    }

    @FXML
    private void revenge() {

    }


    @FXML
    private void exit() {
        List<Scene> scenes = new ArrayList<>();
        scenes.add(ScreenManager.getInstance().getScenes().get(ScreenManager.SCREEN_LOGIN_SCENE));
        ScreenManager.getInstance().setScenes(scenes);
        GameManager.getInstance().getGameLogicThread().setRuning(false);
        ThreadManager.getInstance().setEnemyUser(null);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_FLEET_SCENE);
    }
}

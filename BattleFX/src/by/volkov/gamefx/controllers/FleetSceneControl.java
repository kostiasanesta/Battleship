package by.volkov.gamefx.controllers;


import by.seabattle.messaging.messages.GetOnlineUsersMessage;
import by.volkov.gamefx.core.GameManager;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import by.volkov.gamefx.gamecontrolls.GameCell;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.io.IOException;

public class FleetSceneControl extends BaseController {
    @FXML
    private Button readyBtn;
    @FXML
    private Label playerLabel;
    @FXML
    private Canvas playerCanvas;
    private boolean isRuning;

    @FXML
    private void ready() {
        isRuning=false;
        GetOnlineUsersMessage message = new GetOnlineUsersMessage();
        message.setClientName(ThreadManager.getInstance().getClientThread().getClientName());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        ThreadManager.getInstance().setClientThreadDelegate(this);
        playerLabel.setText(ThreadManager.getInstance().getClientThread().getClientName()+" create your fleet!");
        GameManager.getInstance().initGameLogicThread();
        GameManager.getInstance().initGame(playerCanvas.getGraphicsContext2D(), GameManager.getInstance().getGameLogicThread().getMyCells(), playerCanvas.getHeight() / 11);
        playerCanvas.addEventHandler(MouseEvent.MOUSE_CLICKED, this::onPlayerCanvasClick);
        Thread thread = new Thread(new ButtonSetter());
        thread.start();

    }


    @Override
    protected void processGetOnlineUsersMessage(GetOnlineUsersMessage message) {
        ThreadManager.getInstance().createOnlineUsersList(message.getOnlineUsers());
        ThreadManager.getInstance().savePlayerOnlineUser(message.getClientName());
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_MAIN_SCENE);
    }

    private void onPlayerCanvasClick(MouseEvent event) {
        if (event.getX() > playerCanvas.getHeight() / 11 && event.getY() > playerCanvas.getHeight() / 11) {
            Point2D point2D = new Point2D(event.getX(), event.getY());
            GameManager.getInstance().getGameLogicThread().proccesPlayerCanvasClick(point2D, ((Canvas) event.getSource()).getWidth() / 11);
            for (GameCell[] gameCells : GameManager.getInstance().getGameLogicThread().getMyCells()) {
                for (GameCell gameCell : gameCells) {
                    gameCell.draw(playerCanvas.getGraphicsContext2D());
                }
            }
        }
    }

    @FXML
    private void autoDraw() {
        clear();
        GameManager.getInstance().getGameLogicThread().autoDraw();
        for (GameCell[] gameCells : GameManager.getInstance().getGameLogicThread().getMyCells()) {
            for (GameCell gameCell : gameCells) {
                gameCell.draw(playerCanvas.getGraphicsContext2D());
            }
        }
    }

    @FXML
    private void clear() {
        GameManager.getInstance().getGameLogicThread().clear(playerCanvas.getHeight() / 11);
        for (GameCell[] gameCells : GameManager.getInstance().getGameLogicThread().getMyCells()) {
            for (GameCell gameCell : gameCells) {
                gameCell.draw(playerCanvas.getGraphicsContext2D());
            }
        }
    }

    private class ButtonSetter implements Runnable {
        @Override
        public void run() {
            isRuning = true;
            while (isRuning) {
                if (GameManager.getInstance().getGameLogicThread().getPlayerFleet().isReadyToFight()) {
                    readyBtn.setVisible(true);
                } else {
                    readyBtn.setVisible(false);
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    isRuning = false;
                }

            }
        }
    }
}

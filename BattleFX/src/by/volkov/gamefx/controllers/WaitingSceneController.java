package by.volkov.gamefx.controllers;


import by.seabattle.messaging.messages.GameResponseMessage;
import by.volkov.gamefx.core.GameManager;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import javafx.application.Platform;
import javafx.fxml.FXML;

public class WaitingSceneController extends BaseController {

    @FXML
    public void initialize(){
        ThreadManager.getInstance().setClientThreadDelegate(this);
    }

    @Override
    protected void processGameResponseMessage(GameResponseMessage message) {
        Platform.runLater(() -> {
            ScreenManager.getInstance().closeModalityStage();
        });
        if(message.isApproved()){
            GameManager.getInstance().getGameLogicThread().setYourTurn(message.isFirstTurn());
            GameManager.getInstance().getGameLogicThread().setCanFire(message.isFirstTurn());
            ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_GAME_SCENE);
        }else {
            ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_MAIN_SCENE);
        }
    }
}

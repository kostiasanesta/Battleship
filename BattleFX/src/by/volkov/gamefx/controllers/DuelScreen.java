package by.volkov.gamefx.controllers;


import by.seabattle.messaging.messages.GameResponseMessage;
import by.volkov.gamefx.core.GameManager;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.io.IOException;
import java.util.Random;

public class DuelScreen extends BaseController {
    private static String chalanger;

    @FXML
    private Label msgLbl;

    @FXML
    private void accept() {
        sendResponse(true);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_GAME_SCENE);
    }

    @FXML
    private void decline() {
        sendResponse(false);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_MAIN_SCENE);
    }

    @FXML
    public void initialize() {
        ThreadManager.getInstance().getClientThread().setDelegate(this);
        msgLbl.setText(chalanger+" chalanges you!");
    }

    public DuelScreen() {
        System.out.println("smth");
    }

    private void sendResponse(boolean letsPlay) {
        GameResponseMessage message = new GameResponseMessage();
        if(letsPlay){
            Random random = new Random();
            boolean firstTurn = random.nextBoolean();
            System.out.println(" bolean "+ firstTurn);
            GameManager.getInstance().getGameLogicThread().setYourTurn(firstTurn);
            GameManager.getInstance().getGameLogicThread().setCanFire(firstTurn);
            message.setFirstTurn(!(firstTurn));
        }
        message.setApproved(letsPlay);
        message.setRecipientId(ThreadManager.getInstance().getRecipientId());
        System.out.println(ThreadManager.getInstance().getRecipientId());
        System.out.println(ThreadManager.getInstance().getPlayerUser().getUserId());
        message.setSenderId(ThreadManager.getInstance().getPlayerUser().getUserId());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ScreenManager.getInstance().closeModalityStage();
    }

    public static void setChalanger(String chalanger) {
        DuelScreen.chalanger = chalanger;
    }
}

package by.volkov.gamefx.controllers;

import by.seabattle.messaging.messages.GameRequestMessage;
import by.seabattle.messaging.messages.GetOnlineUsersMessage;
import by.seabattle.messaging.messages.WaitingMessage;
import by.seabattle.messaging.units.OnlineUser;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.io.IOException;


public class MainSceneController extends BaseController {
    @FXML
    private VBox vBoxNames;
    @FXML
    private VBox vBoxBtns;
    @FXML
    private Button refreshBtn;
    @FXML
    private Label label;
    private Button battleBtn;


    @FXML
    private void refresh() {
        GetOnlineUsersMessage message = new GetOnlineUsersMessage();
        message.setClientName(ThreadManager.getInstance().getClientThread().getClientName());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void initialize() {
        ThreadManager.getInstance().setClientThreadDelegate(this);
        for (OnlineUser user : ThreadManager.getInstance().getOnlineUsers()) {
            if (user.getName() == ThreadManager.getInstance().getPlayerUser().getName()) {
                this.label.setText(ThreadManager.getInstance().getClientThread().getClientName() + " choose your enemy!");
            } else {
                Label label = new Label(user.getName());
                battleBtn = new Button(user.getName());
                battleBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        System.out.println(user.getName());
                        System.out.println(user.getUserId());
                        play(user.getUserId(),user);
                    }
                });
                vBoxNames.getChildren().add(label);
                vBoxBtns.getChildren().add(battleBtn);
            }
        }
    }

    private void play(String userId, OnlineUser enemyUser) {
        ThreadManager.getInstance().setEnemyUser(enemyUser);
        GameRequestMessage msg = new GameRequestMessage();
        ThreadManager.getInstance().setRecipientId(userId);
        msg.setRecipientId(userId);
        msg.setSenderId(ThreadManager.getInstance().getPlayerUser().getUserId());
        msg.setOnlineUser(ThreadManager.getInstance().getPlayerUser());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void processGetRequestMessage(GameRequestMessage message) {
        ThreadManager.getInstance().setRecipientId(message.getSenderId());
        WaitingMessage msg = new WaitingMessage();
        msg.setRecipientId(message.getSenderId());
        msg.setSenderId(message.getRecipientId());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ThreadManager.getInstance().setEnemyUser(message.getOnlineUser());
        DuelScreen.setChalanger(message.getOnlineUser().getName());
        ScreenManager.getInstance().createModalityScreen(ScreenManager.SCREEN_DUEL_SCENE);
    }

    @Override
    protected void processWaitingMessage(WaitingMessage message) {
        ScreenManager.getInstance().createModalityScreen(ScreenManager.SCREEN_WAITING_SCENE);
    }

    public MainSceneController() {
        //  Thread thread = new Thread(new Refresher());
        // thread.start();
    }

    @Override
    protected void processGetOnlineUsersMessage(GetOnlineUsersMessage message) {
        ThreadManager.getInstance().createOnlineUsersList(message.getOnlineUsers());
        ScreenManager.getInstance().getScenes().remove(ScreenManager.SCREEN_MAIN_SCENE);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_MAIN_SCENE);
    }

    private class Refresher implements Runnable {
        boolean isRuning;

        @Override
        public void run() {
            isRuning = true;
            while (isRuning) {
                refresh();
                try {
                    Thread.sleep(999999999);
                } catch (InterruptedException e) {
                    isRuning = false;
                }
            }
        }
    }
}
package by.volkov.gamefx.controllers;


import by.seabattle.messaging.messages.BeatMessage;
import by.seabattle.messaging.messages.GameOverMessage;
import by.volkov.gamefx.core.GameManager;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import by.volkov.gamefx.gamecontrolls.GameCell;
import by.volkov.gamefx.gamecontrolls.GameCellStatus;
import by.volkov.gamefx.gamecontrolls.GameCellType;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.io.IOException;


public class GameSceneController extends BaseController {
    @FXML
    private Label playerLabel;
    @FXML
    private Label enemyLabel;
    @FXML
    private Button surrendBtn;
    @FXML
    private Canvas playerCanvas;
    @FXML
    private Canvas enemyCanvas;


    @FXML
    private void surrend() {
        GameOverMessage message = new GameOverMessage();
        message.setRecipientId(ThreadManager.getInstance().getEnemyUser().getUserId());
        message.setSenderId(ThreadManager.getInstance().getPlayerUser().getUserId());
        try {
            ThreadManager.getInstance().getClientThread().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_OVER_SCENE);
    }

    @FXML
    public void initialize() {
        ThreadManager.getInstance().setClientThreadDelegate(this);
        playerLabel.setText(ThreadManager.getInstance().getPlayerUser().getName());
        enemyLabel.setText(ThreadManager.getInstance().getEnemyUser().getName());
        GameManager.getInstance().getBuilder().drawNumbersAndLetters(playerCanvas.getGraphicsContext2D(), playerCanvas.getHeight() / 11);
        for (GameCell[] gameCells : GameManager.getInstance().getGameLogicThread().getMyCells()) {
            for (GameCell gameCell : gameCells) {
                gameCell.draw(playerCanvas.getGraphicsContext2D());
            }
        }
        GameManager.getInstance().getBuilder().drawNumbersAndLetters(enemyCanvas.getGraphicsContext2D(), enemyCanvas.getHeight() / 11);
        for (GameCell[] gameCells : GameManager.getInstance().getGameLogicThread().getEnemyCells()) {
            for (GameCell gameCell : gameCells) {
                gameCell.draw(enemyCanvas.getGraphicsContext2D());
            }
        }
        enemyCanvas.addEventHandler(MouseEvent.MOUSE_CLICKED, this::onEnemyCanvasClick);
        System.out.println("your turn " + GameManager.getInstance().getGameLogicThread().isYourTurn());
        System.out.println("you can fire " + GameManager.getInstance().getGameLogicThread().isCanFire());
    }

    private void onEnemyCanvasClick(MouseEvent event) {
        if (event.getX() > playerCanvas.getHeight() / 11 && event.getY() > playerCanvas.getHeight() / 11) {
            Point2D point2D = new Point2D(event.getX(), event.getY());
            int i = GameManager.getInstance().getGameLogicThread().processOfFire(point2D, ((Canvas) event.getSource()).getWidth() / 11);
            if (i != 1000) {
                BeatMessage message = new BeatMessage();
                message.setCellIndex(i);
                message.setRecipientId(ThreadManager.getInstance().getEnemyUser().getUserId());
                message.setSenderId(ThreadManager.getInstance().getPlayerUser().getUserId());
                try {
                    ThreadManager.getInstance().getClientThread().sendMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void processBeatMessage(BeatMessage message) {
        if (GameManager.getInstance().getGameLogicThread().isYourTurn()) {
            if (message.isDestroyed() && message.isShip() && message.isDamaged()) {
                GameManager.getInstance().getGameLogicThread().lookAtTheResultOfShooting(message.getCellIndex(), message.isDamaged(), message.isDestroyed());
                reDraw(GameManager.getInstance().getGameLogicThread().getEnemyCells(), enemyCanvas);
                GameManager.getInstance().getGameLogicThread().setCanFire(true);
            } else if (message.isDestroyed() && !(message.isShip()) && message.isDamaged()) {
                GameManager.getInstance().getGameLogicThread().lookAtTheResultOfShooting(message.getCellIndex(), false, true);
                reDraw(GameManager.getInstance().getGameLogicThread().getEnemyCells(), enemyCanvas);
                GameManager.getInstance().getGameLogicThread().setCanFire(true);
            } else if (!(message.isDestroyed()) && message.isShip() && message.isDamaged()) {
                GameManager.getInstance().getGameLogicThread().lookAtTheResultOfShooting(message.getCellIndex(), true, false);
                reDraw(GameManager.getInstance().getGameLogicThread().getEnemyCells(), enemyCanvas);
                GameManager.getInstance().getGameLogicThread().setCanFire(true);
            } else if (!(message.isDestroyed()) && !(message.isShip()) && !(message.isDamaged())) {
                GameManager.getInstance().getGameLogicThread().lookAtTheResultOfShooting(message.getCellIndex(), false, false);
                reDraw(GameManager.getInstance().getGameLogicThread().getEnemyCells(), enemyCanvas);
                BeatMessage msg = new BeatMessage();
                msg.setRecipientId(message.getSenderId());
                msg.setSenderId(message.getRecipientId());
                msg.setTurnChanger(true);
                GameManager.getInstance().getGameLogicThread().setYourTurn(false);
                try {
                    ThreadManager.getInstance().getClientThread().sendMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (message.isTurnChanger()) {
                GameManager.getInstance().getGameLogicThread().setCanFire(true);
                GameManager.getInstance().getGameLogicThread().setYourTurn(true);

            } else {
                GameManager.getInstance().getGameLogicThread().checkTheHit(message.getCellIndex());
                reDraw(GameManager.getInstance().getGameLogicThread().getMyCells(), playerCanvas);
                if (GameManager.getInstance().getGameLogicThread().getShotTarget().getStatus() == GameCellStatus.SHIP_DESTROYED) {
                    for (GameCell cell : GameManager.getInstance().getGameLogicThread().getCellsToSend()) {
                        BeatMessage msg = new BeatMessage();
                        msg.setRecipientId(message.getSenderId());
                        msg.setSenderId(message.getRecipientId());
                        msg.setCellIndex(cell.getIndex());
                        msg.setDamaged(true);
                        msg.setDestroyed(true);
                        if (cell.getType() != GameCellType.WATER) {
                            msg.setShip(true);
                        }
                        try {
                            ThreadManager.getInstance().getClientThread().sendMessage(msg);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    GameManager.getInstance().getGameLogicThread().getCellsToSend().clear();
                    if (GameManager.getInstance().getGameLogicThread().getPlayerFleet().getFleet().size() == 0) {
                        surrend();
                    }
                } else {
                    BeatMessage msg = new BeatMessage();
                    msg.setRecipientId(message.getSenderId());
                    msg.setSenderId(message.getRecipientId());
                    msg.setCellIndex(message.getCellIndex());
                    msg.setDamaged(GameManager.getInstance().getGameLogicThread().getShotTarget().isDamaged());
                    if (GameManager.getInstance().getGameLogicThread().getShotTarget().getType() != GameCellType.WATER) {
                        msg.setShip(true);
                    }
                    msg.setDestroyed(false);
                    try {
                        ThreadManager.getInstance().getClientThread().sendMessage(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }




    private void reDraw(GameCell[][] cell, Canvas canvas) {
        for (GameCell[] gameCells : cell) {
            for (GameCell targetCell : gameCells) {
                targetCell.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    @Override
    protected void processGameOverMessage(GameOverMessage message) {
        GameManager.getInstance().getGameLogicThread().setWinner(true);
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_OVER_SCENE);
    }
}

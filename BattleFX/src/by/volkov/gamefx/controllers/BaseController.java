package by.volkov.gamefx.controllers;


import by.seabattle.messaging.interfaces.IMessageDispatcher;
import by.seabattle.messaging.messages.*;

public abstract class BaseController implements IMessageDispatcher {
    @Override
    public void onMessageReceived(Message message) {
        switch (message.getType()) {
            case Message.MSG_TYPE_ONLINE_USERS:
                processGetOnlineUsersMessage((GetOnlineUsersMessage) message);
                break;
            case Message.MSG_TYPE_GAME_REQUEST:
                processGetRequestMessage((GameRequestMessage) message);
                break;
            case Message.MSG_TYPE_GAME_RESPONSE:
                processGameResponseMessage((GameResponseMessage) message);
                break;
            case Message.MSG_TYPE_GAME_OVER:
                processGameOverMessage((GameOverMessage) message);
                break;
            case Message.MSG_TYPE_BEAT:
                processBeatMessage((BeatMessage) message);
                break;
            case Message.MSG_TYPE_WAITING:
                processWaitingMessage((WaitingMessage) message);
                break;
            case Message.MSG_TYPE_FLEET:
                processGameFleetMessage((GameFleetMessage) message);
                break;
        }
    }

    protected void processGameFleetMessage(GameFleetMessage message) {
    }

    protected void processWaitingMessage(WaitingMessage message) {
    }

    protected void processGetOnlineUsersMessage(GetOnlineUsersMessage message) {
    }

    protected void processGetRequestMessage(GameRequestMessage message) {
    }

    protected void processGameResponseMessage(GameResponseMessage message) {
    }

    protected void processGameOverMessage(GameOverMessage message) {
    }

    protected void processBeatMessage(BeatMessage message) {
    }
}


package by.volkov.gamefx.controllers;


import by.seabattle.messaging.messages.GameFleetMessage;
import by.volkov.gamefx.core.ScreenManager;
import by.volkov.gamefx.core.ThreadManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;

public class LoginScreenController extends BaseController{
    @FXML
    private TextField nameId;
    @FXML
    private TextField ipAdress;
    @FXML
    private TextField port;
    @FXML
    private Button btnConnect;

    @FXML
    private void connect(){
        try {
            ThreadManager.getInstance().initClientThread(nameId.getText(), ipAdress.getText(),Integer.parseInt(port.getText()));
            ThreadManager.getInstance().setClientThreadDelegate(this);
           GameFleetMessage message = new GameFleetMessage();
            ThreadManager.getInstance().getClientThread().sendMessage(message);
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }

    }



    @Override
    protected void processGameFleetMessage(GameFleetMessage message) {
        ScreenManager.getInstance().switchScene(ScreenManager.SCREEN_FLEET_SCENE);
    }



    @FXML
    public void initialize(){

    }


}

